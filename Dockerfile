FROM alpine:edge

RUN apk --no-cache add minidlna

ADD minidlna.conf /etc/minidlna.conf

ADD entrypoint.sh /entrypoint.sh

ARG DEPLOY_TOKEN_VALUE="$DEPLOY_TOKEN_VALUE"
ENV DEPLOY_TOKEN_VALUE="$DEPLOY_TOKEN_VALUE"


HEALTHCHECK --interval=20s --timeout=3s --start-period=3s \
  CMD curl -f http://127.0.0.1:8200/ || exit 1

HEALTHCHECK --interval=1m --timeout=3s \
  CMD test -f /etc/minidlna.conf || exit 1

ENTRYPOINT [ "./entrypoint.sh"]
