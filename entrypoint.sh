#!/bin/sh

PID_FILE="/run/minidlna/minidlna.pid"

[ -f $PIDFILE ] && rm -f $PIDFILE

exec /usr/sbin/minidlnad -S
